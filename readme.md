# Excluding a plugin

Open the proper `*-exclude.txt` file and add the folder name of the plugin you want to exclude from Jenkins jobs with a leading `/` on a new line.

Example: My plugin is named `gravity-forms`, I would add a new line with `/gravity-forms`.

---------
# Setting up GitLab deploy

- Move repo to the correct plugin space in GitLab
- Add plugin slug to the proper exclude file in https://git.doit.wisc.edu/SABERG3/plugin-migration-exclude
- Add a GitLab CI file to your plugin project (`.gitlab-ci.yml`)

```yml
include:
  - project: "wiscweb/automation/configs"
    file:
      - "plugins/plugin-base.yml"
      # - "deploy/wwcms.yml"
      # - "deploy/cals.yml"
      # - "deploy/calswebh.yml"
      # - "deploy/soeadm.yml"

      # - "deploy/notification.yml"
      #    ^ Use this partial to send a notification to the WiscWeb Teams channel on tag creation!
```
- Un-comment out the deploy partials for the hosts you wish to deploy to.
